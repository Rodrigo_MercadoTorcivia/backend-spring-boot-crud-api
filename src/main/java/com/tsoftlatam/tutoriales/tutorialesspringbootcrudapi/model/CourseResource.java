package com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;



@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
public class CourseResource {

	  
	  @Autowired
	  private CourseRepository courseRepository;

	  @GetMapping("/courses") 
	  public ResponseEntity<List<Course>> getAllCourses() { 
		  try {
			  List<Course> courses = new ArrayList<Course>();
			  courseRepository.findAll().forEach(courses::add);
			  if (courses.isEmpty()) {
					return new ResponseEntity<>(HttpStatus.NO_CONTENT);
				}

				return new ResponseEntity<>(courses, HttpStatus.OK);
		  }catch(Exception e) {
			  return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		  }
	  }
	  
	  @GetMapping("/instructors/{username}/courses") 
	  public ResponseEntity<List<Course>> getAllByUsername(@PathVariable String username) { 
		  try {
			  List<Course> courses = new ArrayList<Course>();
			  courseRepository.findByUsername(username).forEach(courses::add);
			  if (courses.isEmpty()) {
					return new ResponseEntity<>(HttpStatus.NO_CONTENT);
				}

				return new ResponseEntity<>(courses, HttpStatus.OK);
		  }catch(Exception e) {
			  return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		  }
	  }
	  
	  @DeleteMapping("/courses/{id}")
	  public ResponseEntity<Void> deleteCourse(@PathVariable long id) {

	    try {
	    	courseRepository.deleteById(id);
	    	return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    } catch(Exception e){
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	  }
	  
	  @DeleteMapping("/instructors/{username}/courses")
	  public ResponseEntity<Void> deleteCoursesByInstructor(@PathVariable String username) {

	    try {
	    	List<Course> coursesToDelete = new ArrayList<Course>();
	    	courseRepository.findByUsername(username).forEach(coursesToDelete::add);
	    	for (Course course : coursesToDelete) {
	    		courseRepository.deleteById(course.getId());
			}
	    	return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    } catch(Exception e){
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	  }
	  
	 /* @DeleteMapping("/instructors/{username}/courses") 
	  public ResponseEntity<List<Course>> deleteAllByUsername(@PathVariable String username) { 
		  try {
			  courseRepository.deleteByUsername(username);
			  return new ResponseEntity<>(HttpStatus.NO_CONTENT);
				
		  }catch(Exception e) {
			  return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		  }
	  }*/
	  
	  @GetMapping("/courses/{id}")
	  public ResponseEntity<Course> getCourse(@PathVariable long id) {
	    //return courseManagementService.findById(id);
		  Optional<Course> courseData = courseRepository.findById(id);
			
			if (courseData.isPresent()) {
				return new ResponseEntity<>(courseData.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
	  }
	  
	  @PutMapping("/courses/{id}")
	  public ResponseEntity<Course> updateCourse(@PathVariable long id,
	      @RequestBody Course course) {

	    /*Course courseUpdated = courseManagementService.save(course);

	    return new ResponseEntity<Course>(courseUpdated, HttpStatus.OK);*/
		  Optional<Course> courseData =  courseRepository.findById(id);
			if(courseData.isPresent()) {
				Course _courseData = courseData.get();
				_courseData.setUsername(course.getUsername());
				_courseData.setDescription(course.getDescription());
				_courseData.setStudents(course.getStudents());
				return new ResponseEntity<>(courseRepository.save(_courseData),HttpStatus.OK);
			}
			else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
	  }
	  
	  @PostMapping("/instructors/{username}/courses")
	  public ResponseEntity<Course> createCourse(@PathVariable String username,@RequestBody Course course){
		  try {
				Course _course = courseRepository.save(new Course(course.getUsername(),course.getDescription(),course.getStudents()));
				return new ResponseEntity<>(_course,HttpStatus.CREATED);
			} catch(Exception e) {
				return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
			}
	  }

}
