package com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;


@Service
public class CoursesHardcodedService {
	  private static List<Course> courses = new ArrayList<>();
	  private static long idCounter = 0;

	  /*static {
		    courses.add(new Course(++idCounter, "tsoft", "Learn Full stack with Spring Boot and Angular4",4));
		    courses.add(new Course(++idCounter, "tsoft", "Learn Full stack with Spring Boot and React3",10));
		    courses.add(new Course(++idCounter, "tsoft", "Master Microservices with Spring Boot and Spring Cloud2",0));
		    courses.add(new Course(++idCounter, "tsoft",
		        "Deploy Spring Boot Microservices to Cloud with Docker and Kubernetes1",20));
		  }*/

	  public List<Course> findAll() {
	    return courses;
	  }
	  
	  public Course findById(long id) {
		  for (Course course: courses) {
		    if (course.getId() == id) {
		      return course;
		    }
		  }
		  return null;
	  }
	  public Course deleteById(long id) {
		    Course course = findById(id);

		    if (course == null)
		      return null;

		    if (courses.remove(course)) {
		      return course;
		    }

		    return null;
	  }
	  
	  public Course save(Course course) {
		  if (course.getId() == -1 || course.getId() == 0) {
		    course.setId(++idCounter);
		    courses.add(course);
		  } else {
		    deleteById(course.getId());
		    courses.add(course);
		  }
		  return course;
		}

}
