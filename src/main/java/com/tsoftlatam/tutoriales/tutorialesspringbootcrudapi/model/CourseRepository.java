package com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CourseRepository extends JpaRepository<Course, Long>{

	//List<Course> findByPublished(boolean published);
	List<Course> findByUsername(String username);
	long deleteByUsername(String username);
	List<Course> findByDescriptionContainingIgnoreCaseOrUsernameContainingIgnoreCase(String description,String username);
	
}
