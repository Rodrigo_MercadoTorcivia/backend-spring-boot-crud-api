package com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.courseinstructor;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.instructors.model.Instructor;
import com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.instructors.repository.InstructorRepository;
import com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.model.Course;
import com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.model.CourseRepository;


@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
public class CourseInstructorController {
	
	@Autowired
	InstructorRepository instructorRepository;
	
	@Autowired
	CourseRepository courseRepository;
	
	
	@GetMapping("/search/{text}")
	public ResponseEntity<List<CourseInstructor>> getAllCoincidences(@PathVariable String text){
		
		try {
			
				List<Course> courses = new ArrayList<Course>();
				List<Instructor> instructors = new ArrayList<Instructor>();
				instructorRepository.findByUsernameContainingIgnoreCaseOrNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(text,text,text).forEach(instructors::add);
				courseRepository.findByDescriptionContainingIgnoreCaseOrUsernameContainingIgnoreCase(text,text).forEach(courses::add);
			  List<CourseInstructor> coincidences = new ArrayList<CourseInstructor>();
			  for(int i=0;i<courses.size();i++) {
// int index, Long id, String username, String descOrNameLastname, String typeObject
				  Course _course = courses.get(i);
				  CourseInstructor courseInstructor = new CourseInstructor(i,_course.getId(),_course.getUsername(),_course.getDescription(),"course");
				  coincidences.add(courseInstructor);
			  }
			  System.out.println("texto enviado - " + courses.size() + instructors.size());
			  
			  for(int i=0;i<instructors.size();i++) {
				Instructor _instructor = instructors.get(i);
				CourseInstructor courseInstructor = new CourseInstructor(coincidences.size(),_instructor.getId(),_instructor.getUsername(),_instructor.getName() + " " + _instructor.getLastName(),"instructor");
				coincidences.add(courseInstructor);
			  }
			  System.out.println("texto enviado - " + courses.size() + instructors.size() + " coincide - " + coincidences.size());
			  if (coincidences.isEmpty()) {
					return new ResponseEntity<>(HttpStatus.NO_CONTENT);
					
				}
			  
				return new ResponseEntity<>(coincidences, HttpStatus.OK);
		  }catch(Exception e) {
			  return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		  }
	}
	
	
	@GetMapping("/search")
	public ResponseEntity<List<CourseInstructor>> getAll(){
		
		try {
			
				List<Course> courses = new ArrayList<Course>();
				List<Instructor> instructors = new ArrayList<Instructor>();
				instructorRepository.findAll().forEach(instructors::add);
				courseRepository.findAll().forEach(courses::add);
			  List<CourseInstructor> coincidences = new ArrayList<CourseInstructor>();
			  for(int i=0;i<courses.size();i++) {
// int index, Long id, String username, String descOrNameLastname, String typeObject
				  Course _course = courses.get(i);
				  CourseInstructor courseInstructor = new CourseInstructor(i,_course.getId(),_course.getUsername(),_course.getDescription(),"course");
				  coincidences.add(courseInstructor);
			  }
			  //System.out.println("texto enviado - " + courses.size() + instructors.size());
			  
			  for(int i=0;i<instructors.size();i++) {
				Instructor _instructor = instructors.get(i);
				CourseInstructor courseInstructor = new CourseInstructor(coincidences.size(),_instructor.getId(),_instructor.getUsername(),_instructor.getName() + " " + _instructor.getLastName(),"instructor");
				coincidences.add(courseInstructor);
			  }
			  //System.out.println("texto enviado - " + courses.size() + instructors.size() + " coincide - " + coincidences.size());
			  if (coincidences.isEmpty()) {
					return new ResponseEntity<>(HttpStatus.NO_CONTENT);
					
				}
			  
				return new ResponseEntity<>(coincidences, HttpStatus.OK);
		  }catch(Exception e) {
			  return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		  }
	}
	
	
	
}
