package com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.courseinstructor;

public class CourseInstructor {
	
	private int index;
	private Long id;
	private String username;
	private String descOrNameLastname;
	private String typeObject;
	
	public CourseInstructor() {
		super();
	}

	public CourseInstructor(int index, Long id, String username, String descOrNameLastname, String typeObject) {
		super();
		this.index = index;
		this.id = id;
		this.username = username;
		this.descOrNameLastname = descOrNameLastname;
		this.typeObject = typeObject;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDescOrNameLastname() {
		return descOrNameLastname;
	}

	public void setDescOrNameLastname(String descOrNameLastname) {
		this.descOrNameLastname = descOrNameLastname;
	}

	public String getTypeObject() {
		return typeObject;
	}

	public void setTypeObject(String typeObject) {
		this.typeObject = typeObject;
	}
	
	
	
	
}
