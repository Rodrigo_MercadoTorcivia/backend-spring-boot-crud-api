package com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorialesSpringBootCrudApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TutorialesSpringBootCrudApiApplication.class, args);
	}

}
