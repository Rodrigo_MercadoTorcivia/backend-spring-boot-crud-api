package com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.instructors.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.instructors.model.Instructor;
import com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.instructors.repository.InstructorRepository;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
public class InstructorController {

	@Autowired
	InstructorRepository instructorRepository;
	
	@GetMapping("/instructors")
	public ResponseEntity<List<Instructor>> getAllInstructors() { 
		  try {
			  List<Instructor> instructors = new ArrayList<Instructor>();
			  instructorRepository.findAll().forEach(instructors::add);
			  if (instructors.isEmpty()) {
					return new ResponseEntity<>(HttpStatus.NO_CONTENT);
				}
		
				return new ResponseEntity<>(instructors, HttpStatus.OK);
		  }catch(Exception e) {
			  return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		  }
	 }
	
	/*@GetMapping("/instructors/{id}")
	public ResponseEntity<Instructor> getInstructor(@PathVariable Long id){
		Optional<Instructor> instructorData = instructorRepository.findById(id);
		
		if(instructorData.isPresent()) {
			return new ResponseEntity<>(instructorData.get(),HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		}
	}*/
	
	@GetMapping("/instructors/{username}")
	public ResponseEntity<Instructor> getInstructorByUsername(@PathVariable String username){
		try {
			  List<Instructor> instructors = instructorRepository.findByUsername(username);
			  //instructorRepository.findByUsername(username).forEach(instructors::add);
			  if (instructors== null) {
					return new ResponseEntity<>(HttpStatus.NO_CONTENT);
				}
			  Instructor returnInstructor = instructors.get(0);
			  System.out.println(returnInstructor.getLastName() + returnInstructor.getUsername());
				return new ResponseEntity<>(returnInstructor, HttpStatus.OK);
		  }catch(Exception e) {
			  return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		  }
		/*System.out.println(username);
		List<Instructor> instructors = instructorRepository.findByUsername(username);
		for(int i = 0; i< instructors.size();i++) {
			System.out.println(instructors.get(i).getUsername());
		}
		
		 return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);*/
	}
	
	@DeleteMapping("/instructors/{username}")
	public ResponseEntity<Void> deleteInstructor(@PathVariable String username){
		List<Instructor> instructors = instructorRepository.findByUsername(username);
		
		try {
			Long id = instructors.get(0).getId();
			instructorRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/instructors")
	public ResponseEntity<Instructor> createInstructor(@RequestBody Instructor instructor){
		try {
			Instructor _insrtuctor = instructorRepository.save(new Instructor(instructor.getUsername(),instructor.getName(),instructor.getLastName()));
			return new ResponseEntity<>(_insrtuctor,HttpStatus.CREATED);
		}catch(Exception e) {
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/instructors/{username}")
	public ResponseEntity<Instructor> updateInstructor(@RequestBody Instructor instructor,@PathVariable String username){
		List<Instructor> instructorData= instructorRepository.findByUsername(username);
		if(!instructorData.isEmpty()) {
			Instructor _instructor = instructorData.get(0);
			_instructor.setUsername(instructor.getUsername());
			_instructor.setName(instructor.getName());
			_instructor.setLastName(instructor.getLastName());
			return new ResponseEntity<>(instructorRepository.save(_instructor),HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	
	
	
}
