package com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.instructors.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tsoftlatam.tutoriales.tutorialesspringbootcrudapi.instructors.model.Instructor;

@Repository
public interface InstructorRepository extends JpaRepository<Instructor, Long>{
	
	
    //ThingEntity findByFooInAndBar(String fooIn, String bar);
    //@Query("SELECT t FROM instructors t WHERE t.username = :username")
	//Instructor findByUsername(@Param("username")String username);
	
	List<Instructor> findByUsername(String username);
	Long deleteByUsername(String username);
	List<Instructor> findByUsernameContainingIgnoreCaseOrNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(String username,String name,String lastname);
}
